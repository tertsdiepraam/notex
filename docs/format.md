# Notex File Format

The Notex file format will a version of YAML.

Why YAML? First of all, YAML is plain text, which makes it ideal for writing
documents in text editors if that is preferred. Compared to JSON & XML, YAML is
also super readable. The indent based syntax also allows for easy embedding of
other languages, which we need. YAML is complex to parse, but luckily good
parsers already exist.

In the description below we write types instead of values. A string is just a
normal string, a URL is a string with a URL, which can be local or on the
internet. A date is simply a string with a date in ISO format.

So what does it look like? Every document starts with some document properties:

``` yaml
document:
    title: string
    authors: string, list of string or something more complex for affiliations and emails
    date: date or None
    styling: (URL to) CSS (with different properties)
```

`title` is used for both the title in the document and the metadata (e.g. both
the `<h1>` and `<title>` tag in HTML).

`authors` needs more work. I want it to be something like:

``` yaml
authors:
    - name: John Doe
      affiliation: string or list of string
      email: string
      ...
    - name: Jane Doe
      affiliation: string or list of string
      email: string
      ...
```

The `date` field should default to the date of compilation.

Much in the spirit of the rest of this project, where established languages and
tools are not discarded, but combined to create something new, we use CSS for
`styling` of the document. There will be a command to inherit CSS from a URL.

Then each plugin gets a block with global definitions, which can be modified by
the user. A plugin can have multiple blocks in any data format they specify. If
a block is missing, then it will be regarded as empty.

``` yaml
global:
    latex: |
        definitions:
            \usepackage{amsmath}
            \newcommand\Natural{\mathbb{N}}
    python: |
        definitions:
            import numpy
            def include_file(path):
                with open(path, 'r') as f:
                    return f.read()
```

Plugins should probably also have the option to use a single block:

``` yaml
global:
    latex: |
        \usepackage{amsmath}
        \newcommand\Natural{\mathbb{N}}
```

