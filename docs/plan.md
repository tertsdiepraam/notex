# Notex: Advanced Typesetting as Simple as Notetaking
(without TeX)

Typesetting is hard, especially with technical documents. If you want to create
a technical document, there are a couple of options:

- LaTeX / TeX / ConTeXt
- Markdown
- Jupyter Notebooks
- Word
- LibreOffice / OpenOffice
- Emacs Org Mode
- Plain HTML

To motivate this project, we first identify the problems that each present,
before we explore solutions. I don't pretend to have all the answers, but I do
want to commit myself to finding them.

First let's lay out some goals. I want a typesetting system that:

- Can output to PDF, EPUB & HTML.
- Has powerful layout capabilities.
- Is scriptable.
- Is WYSIWYG when possible/reasonable.
- Has simple syntax that is quick enough to type for notetaking.
- Has good mathematics support (with custom functions/macros and symbols).
- Is extensible.
- Has default support for advanced features like bibliography, links & footnotes.

Seems like a pipe dream, but hey, we can try.

## Other Approaches
### LaTeX / TeX / ConTeXt
The TeX family of typesetting tools is incredibly powerful and mature. For most
technical documents, it is the obvious choice.

- Not beginner friendly
- A lot of documentation, but PDF is not a great format for docs.
- Hard to customize layout if a package does not exist (ConTeXt is better in
  this regard).
- Many packages are incompatible.
- Extra burden of having to layout both source and output.
- Incomprehensible errors.
- Long compile times.
- No separation between data and layout.
- Macros often lead to unexpected behaviour.
- No first-party support of many standard things (bibliography, links, advanced
  formatting).
- Customizable & Scriptable through macros.
- Professional default style.
- Excellent mathematics.
- EPUB and HTML are not possible.
- Too verbose for quick notetaking.
- LyX and Overleaf are cool.
 
### Markdown
If you want a simple document, Markdown is the best option.

- Simple syntax and easy to learn.
- Many good editors.
- Support for code and mathematics.
- Not much customization.
- No (standard) support for references, footnotes and layout.
- HTML & EPUB support.
- Possibly very powerful through Pandoc.
- It's not WYSIWYG, but is pretty close because of the syntax.
- WYSIWYG is sometimes possible (see Notion)

### Jupyter Notebooks
- Same as Markdown.
- Running code is amazing.
- Extensible.
- Block-based editing is cool.

### Word 
The default option for most people.

- Easy to use, but also easy to abuse.
- Many advanced features are hidden in menu's.
- Not open source.
- Awful mathematics typesetting.
- Encourages direct styling, instead of semantic styling (I'll get into this).

### LibreOffice / OpenOffice
Same as Word but still often less powerful, but points for being open source.

### Emacs Org Mode
- Harder than markdown, but still easy to use.
- Great editor experience.
- Requires LaTeX syntax to do advanced typesetting.
- HTML target is a very good idea.
- Running code is awesome.

### Plain HTML
- Tedious, please don't.

## NoTeX
So what good ideas do we want to steal?

- Scripting and extensions.
- LaTeX mathematics syntax.
- Jupyter notebook blocks are cool.
- Simple syntax, good keyboard shortcuts.
- Python.
- Git integration.

What mistakes do we not want to make?

- Some obscure custom scripting language (looking at you TeX).
- Not separating data from layout (looking at you TeX).
- Verbose syntax (looking at you TeX).
- Closed source (looking at you Word).
- Not scriptable (looking at many options above).
- Limited to PDF (looking at you TeX).

So how?

- Layout engine in Rust.
- Python Scripting.
- Custom editor in Rust.
- Everything is block based like Jupyter.
- Each block is an extensions.
- You could have a TeX block to render some TeX or a figure block or maths or code.
- Blocks are compiled incrementally.
- A block could be Python code that returns an object specifying layout.
- Python functions can be created to create custom layouts.
- Layout variables are handled through dictionaries of values, such as margins,
  fonts etc., not through commands like in TeX.
- Layout engine can compile to PDF, EPUB and HTML.
- SVG as primary graphics engine (Tikz replacement).

There is a Rust layout engine, which holds the state of the document, which can
be stored in JSON or something like that. Through PyO3, the engine runs python
snippets, which return layout objects that the engine can then use. So scripting
does not work through macros. The engine can then be used in multiple ways. For
example, it's possible to build a custom editor on top of it, or a command-line
utility, or expose bindings to Python or some other language again. Maybe even
compile it for webassembly and make it work in the browser.

The layout engine is very general. It works on data structures that are passed
to it by plugins. It does not do any parsing on its own. Then it exports it to
HTML, EPUB, PDF or some other format. Possibly this is also through plugins.

## To think about
- How do we deal with citations, references and inline maths?

## To implement
- Layout engine
 - Data structures & functions to create layouts
 - Custom format parser
 - Incremental compilation
 - Plugin system
 - Exporter
- Layout plugins in Rust or Python
- Python library for specifying layouts
- GUI Editor

## Roadmap
### Minimal Viable Product
- Set up architecture
- Make a markdown plugin (subset of markdown is enough)
- Let it export to HTML
- So actually a contrived markdown to html parser

### Next steps
- Simple editor
- LaTeX math plugin
